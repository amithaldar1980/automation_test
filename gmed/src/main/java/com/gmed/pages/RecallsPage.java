package com.gmed.pages;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.sikuli.script.FindFailed;

import com.gmed.Frames.DynamicFramePage;
import com.gmed.Frames.Frames;
import com.gmed.base.BaseAbstractPage;
import com.gmed.test.ImmunizationTest;
import com.gmed.utils.ConstantsFile;
import com.gmed.utils.DateUtil;
import com.gmed.utils.SeleniumUtil;
import static com.gmed.helper.DriverFactory.driver;
import static com.gmed.helper.DriverFactory.action;

import java.awt.AWTException;
import java.util.Iterator;
import java.util.List;


public class RecallsPage extends BaseAbstractPage {
	private static Logger	logger				= LogManager.getLogger(RecallsPage.class);
	public static By		toMonth				= By.id("dtbDateTo_Month");
	public static By		toDay				= By.id("dtbDateTo_Day");
	public static By		toYear				= By.id("dtbDateTo_Year");
	public static By		patients			= By.id("txtPatient_TextBox");
	public static By		searchButton		= By.id("btnFiltersSearch_Body");
	public static By		provider			= By.id("txtProviderAdd");
	public static By		fromMonth			= By.id("dtbDateFrom_Month");
	public static By		fromDay				= By.id("dtbDateFrom_Day");
	public static By		fromYear			= By.id("dtbDateFrom_Year");
	public static By		plusSignStatus		= By.id("txtStatusAdd");
	public static By		recallRecords		= By.xpath("//table[@id='radRecalls_ctl00']/tbody/tr[contains(@id,'radRecalls_ctl00')]");
	public static String	recallRecordData1	= "//table[@id='radRecalls_ctl00']/tbody/tr[";
	public static String	recallRecordData2	= "]";

	public void addProvider(String providername) {

		switchToParentFrame(Frames.TOOLTIP);
		enterText(DocumentPage.searchmedicationtxtbox, providername);
		click(DocumentPage.searchMedication1);
		sleep(3000);
		click(DocumentPage.select1frompopupframe);
		click(DocumentPage.selectbtnpopupframe);
	}

	public void searchRecallByProviderPatients(String patient, String provider) {
		sleep(3000);
		clearText(toMonth);
		clearText(toDay);
		clearText(toYear);
		clearText(patients);
		enterText(RecallsPage.patients, patient);
		click(RecallsPage.provider);
		addProvider(provider);
		SeleniumUtil.switchToParentFrame(Frames.RECALLS);
		click(RecallsPage.searchButton);
	}

	public void searchRecallByStatus(String status) {
		sleep(3000);
		click(plusSignStatus);
		switchToParentFrame(Frames.TOOLTIP);
		SeleniumUtil.doubleClick(SeleniumUtil.getElementWithFluentWait(By.xpath("//td[text()='" + status + "']")));
		SeleniumUtil.switchToParentFrame(Frames.RECALLS);
		click(RecallsPage.searchButton);
	}

	public boolean verifyRecallStatus(String recalltype, String recalldate, String recallstatus) {
		sleep(3000);
		
		for (int i = 1; i <= SeleniumUtil.getElementCount(recallRecords); i++) {
			try {
				
				if (getElementText(By.xpath(recallRecordData1+i+recallRecordData2)).contains(recalltype) && getElementText(By.xpath(recallRecordData1+i+recallRecordData2)).contains(recalldate) 
						&& getElementText(By.xpath("//table[@id='radRecalls_ctl00']/tbody/tr["+i+"]")).contains(recallstatus)) {
						 
					return true;
				} else {
					if (i == SeleniumUtil.getElementCount(recallRecords)) {
						return false;
					}
				}
			} catch (Exception e) {
				System.out.println("Exception  " + i+"test" +e);
				if (i == SeleniumUtil.getElementCount(recallRecords)) {
					return false;
				}
			}
		}
		return false;
	}

}
