package com.gmed.pages;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.sikuli.script.FindFailed;

import com.gmed.Frames.Frames;

import static com.gmed.helper.DriverFactory.driver;

import com.gmed.base.BaseAbstractPage;
import com.gmed.utils.ConstantsFile;
import com.gmed.utils.SeleniumUtil;

public class OutputManagerPage extends BaseAbstractPage {
	/** Logger to log the OutputManagerPage log messages */
	private static Logger logger                        = LogManager.getLogger(OutputManagerPage.class);
	public static By mapUser                            = By.id("pnlUserTableAdd");
	
	
	
	public void clickOnProcedure(String procedurename) throws Exception{
		SeleniumUtil.switchToParentFrame(Frames.USERPAGE);
		SeleniumUtil.getElementWithFluentWait(Profile.newbutton).click();
		SeleniumUtil.waitForProgressBar(Frames.USERPAGE);
		SeleniumUtil.switchToParentFrame(Frames.POPUPLIST);
		WebElement FirstVisitvalue= SeleniumUtil.getElementWithFluentWait(By.xpath(".//*[starts-with(text(),'"+ procedurename + "')]"));
		SeleniumUtil.scrolltoWebElement(FirstVisitvalue);
		FirstVisitvalue.click();
		SeleniumUtil.waitForProgressBar(Frames.POPUPLIST);
		Thread.sleep(1000);
	}
	/**
	 * This method is used to click configure first visit setting in output manager 
	 * @throws FindFailed 
	 *
	 * 
	 */
	public void clickOnFirstVisitInService() throws FindFailed{
		SeleniumUtil.switchToParentFrame(Frames.USERCREATION);
		List<WebElement> totalrows = driver.findElements(By.xpath("//table[@id='tblServiceDocuments_Table']/tbody/tr"));
		System.out.println("table name size"+totalrows.get(14).getText());
		WebElement table = totalrows.get(14);
		List<WebElement> tdList = table.findElements(By.xpath(".//td/control"));
		System.out.println(tdList.size());
		int i=0;
		for(WebElement e4:tdList){ 
			SeleniumUtil.switchToParentFrame(Frames.USERCREATION);
			System.out.println(e4.getAttribute("type"));
			String id1=e4.getAttribute("id");
			if(i<=1 ){
				System.out.println(e4.getText());
				System.out.println("id text"+id1);
				WebElement trrr =SeleniumUtil.getElementWithFluentWait(By.id(id1+"_Render"));
				trrr.click();
				SeleniumUtil.waitForProgressBar(Frames.USERCREATION);
				i++;
			}
			else if(i==2){
				WebElement trrr =SeleniumUtil.getElementWithFluentWait(By.id(id1));
				trrr.click();
				SeleniumUtil.waitForProgressBar(Frames.USERCREATION);
				SeleniumUtil.switchToParentFrame(Frames.TOOLTIP);
				WebElement activevalue=SeleniumUtil.getElementWithFluentWait(By.xpath(".//*[contains(text(),'My Local Printer')]"));
				activevalue.click();
				SeleniumUtil.waitForProgressBar(Frames.TOOLTIP);
				SeleniumUtil.getElementWithFluentWait(MedicalChartPage.selectbutton).click();
				SeleniumUtil.waitForProgressBar(Frames.TOOLTIP);
				SeleniumUtil.switchToParentFrame(Frames.USERCREATION);
				i++;
			}
			else if(i==3){
				WebElement trrr =SeleniumUtil.getElementWithFluentWait(By.id(id1));
				trrr.click();
				SeleniumUtil.waitForProgressBar(Frames.USERCREATION);
				SeleniumUtil.clickOnImageWithTargetOffsetInSikuli("selectProvider");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				i++;
			}
			else if(i==4){
				WebElement trrr =SeleniumUtil.getElementWithFluentWait(By.id(id1+"_Render"));
				trrr.click();
				SeleniumUtil.waitForProgressBar(Frames.USERCREATION);
				i++;
			}
			else if(i==5){	
				WebElement trrr =SeleniumUtil.getElementWithFluentWait(By.id("input_"+id1+"_Render"));
				trrr.click();
				SeleniumUtil.waitForProgressBar(Frames.USERCREATION);
				i++;
			}
			else if(i==6){
				System.out.println("select button clicked 6 start");
				WebElement trrr =SeleniumUtil.getElementWithFluentWait(By.id("input_"+id1+"_Render"));
				trrr.click();	
				SeleniumUtil.waitForProgressBar(Frames.USERCREATION);
			}
		}
	}
	/**
	 * This method is used to select user for configuring first visit setting in output manager 
	 * @throws FindFailed 
	 * 
	 */
	public void selectUSerForOutPutManager() throws Exception{
		SeleniumUtil.switchToParentFrame(Frames.USERCREATION);
		logger.info("Mapping the new user with created provider...");
		SeleniumUtil.getElementWithFluentWait(mapUser).click();
		SeleniumUtil.waitForProgressBar(Frames.USERCREATION);
		SeleniumUtil.switchToParentFrame(Frames.TOOLTIP);
		logger.info("search the logged in user by first name & last name");
		SeleniumUtil.getElementWithFluentWait(DocumentPage.patientnametextbox).sendKeys(ConstantsFile.userfirstname);
		SeleniumUtil.getElementWithFluentWait(DocumentPage.patientnametextbox).sendKeys(Keys.SPACE);
		SeleniumUtil.getElementWithFluentWait(DocumentPage.patientnametextbox).sendKeys(ConstantsFile.usercompletelastname);
		logger.info("clicking on search button...");
		SeleniumUtil.getElementWithFluentWait(ConfigurationPage.searchbutton).click();
		SeleniumUtil.waitForProgressBar(Frames.TOOLTIP);
		Thread.sleep(5000);
		logger.info("select the filtered row...");
		List<WebElement> checkbox = driver.findElements(By.className("checkboxSelection"));
		checkbox.get(1).click();
		SeleniumUtil.waitForProgressBar(Frames.TOOLTIP);
		SeleniumUtil.getElementWithFluentWait(AppointmentPage.patientrow).click();
		SeleniumUtil.waitForProgressBar(Frames.TOOLTIP);
		logger.info("select the user to map with the created provider...");
		SeleniumUtil.getElementWithFluentWait(MedicalChartPage.selectbutton).click();
		SeleniumUtil.waitForProgressBar(Frames.TOOLTIP);
		SeleniumUtil.switchToParentFrame(Frames.USERCREATION);
		SeleniumUtil.getElementWithFluentWait(By.id("txtLocationsDropDown")).click();
		SeleniumUtil.waitForProgressBar(Frames.USERCREATION);
		SeleniumUtil.switchToParentFrame(Frames.TOOLTIP);
		logger.info("select the mentioned location(Automation) in the excel");
		List<WebElement> locationlistvalue1 = driver.findElements(By.xpath(".//table[@id='tblList_Table']/tbody/tr"));
		System.out.println("The size of location are:"+locationlistvalue1.size());
		if(locationlistvalue1.size()>0){
			logger.info("selecting the automation location" +ConstantsFile.Fulllocationname);
			WebElement locationvalues= SeleniumUtil.getElementWithFluentWait(By.xpath(".//*[contains(text(),'"+ ConstantsFile.Fulllocationname +"')]"));
			SeleniumUtil.scrolltoWebElement(locationvalues);
			locationvalues.click();
			SeleniumUtil.waitForProgressBar(Frames.TOOLTIP);
		}
	}
}
